-- Write Functor instance of functor for Binary Tree
data Tree a = EmptyTree
            | Node (Tree a) a (Tree a)
            deriving (Show, Eq)

instance Functor Tree where
  fmap f (Node l n r) = Node (fmap f l) (f n) (fmap f r)
  fmap _ _ = EmptyTree
-- Write Functor instance for a RoseTree
data RoseTree a = LeafNode a
                | RoseTreeNode a (Forest a) deriving Show
type Forest a = [RoseTree a]

instance Functor RoseTree where
  fmap f (RoseTreeNode a b) = RoseTreeNode (f a) (map (fmap f) b)
  fmap f (LeafNode a) = LeafNode (f a)

-- Define instance for Functor for Either e
-- used new type Either_ because Either already exists
data Either_ e r = Left_ e
                | Right_ r deriving Show
instance Functor (Either_ e) where
  fmap f (Right_ r) = Right_ (f r)
  fmap _ (Left_ d) = Left_ d
